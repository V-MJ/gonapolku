package main
import ("fmt"
	"encoding/xml"
	"net/http"
	"io"
	"time"
	"strings"
	"github.com/bwmarrin/discordgo"
)

//defining muke rss
type Rss struct{
	XMLName		xml.Name	`xml:"rss"`
	Channel		Channel		`xml:"channel"`
}
type Channel struct{
	XMLName		xml.Name	`xml:"channel"`
	Title		string		`xml:"title"`
	Link		string		`xml:"link"`
	Description	string		`xml:"Description"`
	Items		[]Item		`xml:"item"`
}
type Item struct{
	XMLName		xml.Name	`xml:"item"`
	Guid		string		`xml:"guid"`
	Link		string		`xml:"link"`
	Title		string		`xml:"title"`
	Description	string		`xml:"description"`
}
//static variables
var link2 string = "http://ruokalistat.leijonacatering.fi/rss/2/1/c56d6724-f813-e511-892b-78e3b50298fc"

func getMuke()string{
	//get site
	resp, err := http.Get(link2)
	if err != nil {
		fmt.Printf("fuck\n")
		return ""
	}
	defer resp.Body.Close()
	body, err := io.ReadAll(resp.Body)

	//process rss
	var rss Rss
	xml.Unmarshal(body, &rss)
	
	//make a printable message//
	var messageString string
	currentTime := time.Now()
	//header
	messageString = fmt.Sprintf("*%s", rss.Channel.Title)
	//date
	dateSearch := currentTime.Format("2.1.2006")
	messageString = fmt.Sprintf("%s %s*", messageString, dateSearch)
	//muke ruuat
	//varusmises ruuat printattuna viestiin
	striv := dateSearch + " Varusmiesruokalista"	//perus mehekset
	fmt.Printf("%s\n", striv)
	for itemm := range rss.Channel.Items{
		if strings.Contains(rss.Channel.Items[itemm].Title, striv) == true {
			messageString = fmt.Sprintf("%s\n%s", messageString, strings.ReplaceAll(rss.Channel.Items[itemm].Description, ". ", "\n"))
		}
	}
	//skabu ruuat
	stris := dateSearch + " Henkilöstölounaslista"	//skabut
	messageString = fmt.Sprintf("%s\nJa skabut", messageString)
	for itemm := range rss.Channel.Items{
		if strings.Contains(rss.Channel.Items[itemm].Title, stris) == true {
			messageString = fmt.Sprintf("%s\n%s", messageString, strings.ReplaceAll(rss.Channel.Items[itemm].Description, ". ", "\n"))
		}
	}
	
	//return message to print
	return messageString
}

//func RegisterMukeFunc(funct []*discordgo.ApplicationCommand) []*discordgo.ApplicationCommand{
var mukeDesc = discordgo.ApplicationCommand{
	Name:		"muke",
	Description:	"mitä muonituskeskuksesta löytyy",
}
func RegisterMukeFunc(funct []*discordgo.ApplicationCommand) []*discordgo.ApplicationCommand{
	funct = append(funct, &mukeDesc)
	return funct
}

func RegisterMukeHandler(handlers map[string]func(s *discordgo.Session, i *discordgo.InteractionCreate)){
        handlers["muke"] = func(s *discordgo.Session, i *discordgo.InteractionCreate) {
			s.InteractionRespond(i.Interaction, &discordgo.InteractionResponse{
				Type: discordgo.InteractionResponseChannelMessageWithSource,
				Data: &discordgo.InteractionResponseData{
					Content: getMuke(),
			},
		})
	}
}
