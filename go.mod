module gonapolku

go 1.18

require github.com/bwmarrin/discordgo v0.26.1

require (
	github.com/gorilla/websocket v1.5.0 // indirect
	golang.org/x/crypto v0.0.0-20220829220503-c86fa9a7ed90 // indirect
	golang.org/x/sys v0.0.0-20220915200043-7b5979e65e41 // indirect
)
