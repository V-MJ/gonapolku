package main
import (//"golang.org/x/exp/maps"
	"github.com/bwmarrin/discordgo"
)

var sirkkuDesc = discordgo.ApplicationCommand{
	Name:		"sirkku",
	Description:	"söpö sirkku",
}
func RegisterSirkkuFunc(funct []*discordgo.ApplicationCommand) []*discordgo.ApplicationCommand{
	funct = append(funct, &sirkkuDesc)
	return funct
}

func RegisterSirkkuHandler(handlers map[string]func(s *discordgo.Session, i *discordgo.InteractionCreate)){
	//combine 2 maps
	handlers["sirkku"] = func(s *discordgo.Session, i *discordgo.InteractionCreate){
			s.InteractionRespond(i.Interaction, &discordgo.InteractionResponse{
				Type: discordgo.InteractionResponseChannelMessageWithSource,
				Data: &discordgo.InteractionResponseData{
					Content:"```\n"+
					"░░░░░░░░░░░░▌▒▒▄▀▒▒▒▒▌\n"+
					"▄▀▀▀▀▀▄▄░▀▀▌▒▄▀▒▒▒▒▒▒▐\n"+
					"▒▒▒▒▒▒▒▄█▀▀▀▀░▀▀▀▀▄▄▄▐\n"+
					"▒▒▒▒▒▄▀░░░░░░░░░░░░░░▀▄\n"+     
					"▒▒▒▄▀░░░░░░░░░░░░░░░░░░▀▄\n"+   
					"▄▒▒▌░░░░░▄░▌░░░░░░▌▐░░░░░▌\n"+  
					"░▀▐░░░░░▐░▐░▌░░░░▐░▌▌░░░░▐\n"+  
					"░░▌░░░░░▌▌░░▐░░░░▌▄▄▄▌░░░░▌\n"+ 
					"░▐░░░░░▐░▀▀█▀▐░░▐▀▐▌░▐░░░▐\n"+  
					"░▐░░░░░▌░░░█░░▀▄▌░▐▌░▌▀░▐▐\n"+  
					"░░▌░░░░▌░░░█░░░░░░░█░▌░░▌▌\n"+  
					"░█░░░░▀▄░░░▀░░░░░░░▀░▌▒░▀▄▄▄\n"+
					"▐▒░▒░▒░▄▀░░░░░░░░░░░░▐░▒▄▐░▌\n"+
					"░█▒░▒░▒░▀▄░░░░▄▄░░░▄▄▀▒▄▐░▐\n"+
					"▌░▀▐▄▐▀▄▄░▐▀▄▄▄▄▄▀▀▒▄▄▀░▀░▌\n"+
					"░▌░▄▄▀▄░░▀▐▄▄░░▌▀▀▄▀▄▄░░░▐▄▄▄▀▀█▀\n"+
					"░▄▀░░░▀▄▄▀▀░░▀▀▄▀▄░▀░░▀▀▄▀░░░░█\n"+
					"▀░░░░░░▐▒▐▀▄▄▀▌▌▄▀▄▀▄░░░░▀▀▄░█\n"+
					"░░░░░░░▌▒▐░░▄▀▌▌▀▄▄▀▒▌░░░░░▐▀\n"+
					"```",
 				},
		})
	}
}
