package main
import (
	"os"
	"os/signal"
	"syscall"
	"fmt"
	"flag"

	"github.com/bwmarrin/discordgo"
)

var (
	GuildID		= flag.String("guild",	"",	"Test guild ID. If not passed - bot registers commands globally")
	BotToken	= flag.String("token",	"",	"Bot access token")
)

func init() { flag.Parse() }

func main(){
	//flag processing
	discord, err := discordgo.New( "Bot " + *BotToken)
	if err != nil {
		fmt.Println(err)
		return
	}

	//function related variables
	var commands []*discordgo.ApplicationCommand;
	var commandHandlers = make(map[string]func(s *discordgo.Session, i *discordgo.InteractionCreate))
	//register bot functions and handlers
	commands = RegisterSirkkuFunc(commands);	RegisterSirkkuHandler(commandHandlers)
	commands = RegisterTjFunc(commands);		RegisterTjHandler(commandHandlers)
	commands = RegisterMukeFunc(commands);		RegisterMukeHandler(commandHandlers)
	//register bot function with discordgo.
	discord.AddHandler(func(s *discordgo.Session, i *discordgo.InteractionCreate){
		if h, ok := commandHandlers[i.ApplicationCommandData().Name]; ok {
			h(s, i)
		}
	})
	
	//open discord connection
	err = discord.Open()
	if err != nil {
		fmt.Println(err)
		return
	}

	//register bot functions with discord
	registeredCommands := make([]*discordgo.ApplicationCommand, len(commands))
	for i, v := range commands {
		cmd, err := discord.ApplicationCommandCreate(discord.State.User.ID, *GuildID, v)
		if err != nil {
			fmt.Printf("Cannot create '%v' command: %v", v.Name, err)
			return
		}
		registeredCommands[i] = cmd
	}

	sc := make(chan os.Signal, 1)
	signal.Notify(sc, syscall.SIGINT, syscall.SIGTERM, os.Interrupt, os.Kill)
	<-sc
	// Close connections
	for _, v := range registeredCommands {
		err := discord.ApplicationCommandDelete(discord.State.User.ID, *GuildID, v.ID)
		if err != nil {
			fmt.Printf("Cannot delete '%v' command: %v", v.Name, err)
		}
	}
	discord.Close()
	fmt.Printf("shatdown while full of grace")
}
