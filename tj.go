package main
import ("time"
	"strconv"

	"github.com/bwmarrin/discordgo"
)

var iOMV0 = 100.0
var iOMV1 = 0.0
var tjDesc = discordgo.ApplicationCommand{
	Name:        "tj",
	Description: "tj saapumiserä palvelusaika",
	Options: []*discordgo.ApplicationCommandOption{
		{
			Type:		discordgo.ApplicationCommandOptionInteger,
			Name:		"saapumiserä",
			Description:	"saapumiserä: 121, 212, jne",
			MinValue:	&iOMV0,
			MaxValue:	299,
			Required:	false,
		},
		{
			Type:		discordgo.ApplicationCommandOptionInteger,
			Name:		"palvelusaika",
			Description:	"palvelusaika: 168, 255, 347",
			MinValue:	&iOMV1,
			MaxValue:	999999999,
			Required:	false,
		},
	},
}
func RegisterTjFunc(funct []*discordgo.ApplicationCommand) []*discordgo.ApplicationCommand{
	funct = append(funct, &tjDesc)
	return funct
}

func RegisterTjHandler(handlers map[string]func(s *discordgo.Session, i *discordgo.InteractionCreate)){
	handlers["tj"] = func(s *discordgo.Session, i *discordgo.InteractionCreate) {
		options := i.ApplicationCommandData().Options
		optionMap := make(map[string]*discordgo.ApplicationCommandInteractionDataOption, len(options))
		for _, opt := range options {
			optionMap[opt.Name] = opt
		}

		var tjs string = "";

		if opt, ok := optionMap["saapumiserä"]; ok {
			erä   := opt.IntValue()/100-1;
			vuosi := opt.IntValue()%100;
			var pa int64 = 255
			if iopt, ok := optionMap["palvelusaika"]; ok {
				pa = iopt.IntValue();
			}
			var atime int64 = 1609711200
			var ltime int64 = 15721200
			var ctime int64 = time.Now().Unix()
			var ptime int64 = 86400 * pa
			var ttj int64 = (((atime + (ltime * erä) + (vuosi-21) * 31556926) + ptime) - ctime)
			var tjd int64 = ttj / 86400
			tjs = "tj: " + strconv.Itoa(int(opt.IntValue())) + " = " + strconv.Itoa(int(tjd));
		} else {
			tjs = "tj: ∞"
		}

		s.InteractionRespond(i.Interaction, &discordgo.InteractionResponse{
			Type: discordgo.InteractionResponseChannelMessageWithSource,
			Data: &discordgo.InteractionResponseData{
				Content: tjs,
			},
		})
	}
}
