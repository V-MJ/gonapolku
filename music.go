package main
/*import(
	"io/ioutil"
	"fmt"
	"math/rand"
	"time"
	
	"github.com/bwmarrin/discordgo"
	"github.com/bwmarrin/dgvoice"
)
//internal channel used for halting the thread
var musStop	= make(chan bool, 3)
var musEnd	= make(chan bool, 3)
var musQue	= make(chan string, 512)

//variables used for logic
var ldgv *discordgo.VoiceConnection = nil	//local discord go voice connection


func updateLocalDgv(dgv *discordgo.VoiceConnection){
	ldgv = dgv
}

func seedRandom(){
	rand.Seed(time.Now().UnixNano())
}

func playMusic() {
	seedRandom()
	// Start loop and play music from the que if que is empty randomly select music to play
	for {
		if len(musQue) == 0 {
			files, _ := ioutil.ReadDir(musdir)
			f := files[rand.Intn(len(files))]
			fmt.Printf("empty %s\n", f.Name())
			musQue <- f.Name()
			fmt.Printf("%d", len(musQue))
		}
		fi := <- musQue
		fmt.Println("PlayAudioFile:", fi)
		dgvoice.PlayAudioFile(ldgv, fmt.Sprintf("%s/%s", musdir, fi), musStop)
		//katotaan pitääkö musan toisto lopettaa
		if len(musEnd) > 0{
			<- musEnd
			break
		}
	}
	return
}

func queSong(song string){
	musQue <- song
}
func clearQue(){
	for len(musQue) > 0 {
		<-musQue
	}
	seedRandom()
}
func startMusic(){
	go playMusic()
}
func stopMusic(){
	musEnd <- true
	seedRandom()
	musStop <- true
}
func skipMusic(){
	musStop <- true
	seedRandom()
}
func listMusic(){
	files, _ := ioutil.ReadDir("C:/Users/Auzer/Music")
	for _, f := range files {
		ucomm <- f.Name() 
	}
	seedRandom()
}
/*func addMusic(link string){
	cmd := exec.Command("youtube-dl", link)
	err := cmd.Start()
	fmt.Printf("Command with error: %v", err)
}*/
/*
func RegisterMusFuncs(fbm []botmod)[]botmod{
	var nbm botmod
	nbm.prefix		= "!mus"
	nbm.funcmap		= make(map[string]interface{})
	nbm.funcmap["play"]	= startMusic
	nbm.funcmap["skip"]	= skipMusic
	nbm.funcmap["stop"]	= stopMusic
	nbm.funcmap["list"]	= listMusic
	nbm.funcmap["que"]	= queSong
	//nbm.funcmap["add"]	= addMusic
	nbm.funcmap["clear"]	= clearQue

	nbm.help		= "\nmus prefix is reserved for music commands\nplay, skip, stop, list, que, clear, add\nadd only supports sites/links that youtube-dl supports\n"
	return append(fbm, nbm)
}*/
